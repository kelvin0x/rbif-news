import Box from 'components/Box/Box';
import Grid from 'components/Box/Grid';
import { RowBetween, RowFixed, RowMiddle } from 'components/Box/Row';
import Button from 'components/Button/Button';
import Image from 'components/Image';
import Input from 'components/Input/Input';
import Text from 'components/Text/Text';
import React from 'react';
import { styled } from 'styled-components';
import { ContainerSite } from 'theme/styled';

const TopBar = () => {
  return (
    <Wrapper>
      <StyledSocial>
        <ContainerSite>
          <RowBetween>
            <RowFixed flex={1}>
              <Image src='/images/icons/lock.png' width={16} height={16} />
              <Text ml='12px' color='textSubtle'>
                Wednesday, 10 January 2021
              </Text>
            </RowFixed>

            <Grid gridTemplateColumns='1fr 1fr 1fr 1fr' gridGap='12px'>
              <StyledLogoSocial
                src='/images/icons/facebook.png'
                width={24}
                height={24}
              />
              <StyledLogoSocial
                src='/images/icons/twitter.png'
                width={24}
                height={24}
              />
              <StyledLogoSocial
                src='/images/icons/instagram.png'
                width={24}
                height={24}
              />
              <StyledLogoSocial
                src='/images/icons/youtube.png'
                width={24}
                height={24}
              />
            </Grid>
          </RowBetween>
        </ContainerSite>
      </StyledSocial>
      <ContainerSite my='16px !important'>
        <RowBetween py='16px'>
          <StyledWrapLogo>
            <Image
              src='/images/logo.png'
              width={200}
              height={69.6}
              alt='logo-website'
            />
            <Text
              fontSize={'60px'}
              color='primary'
              ml='16px'
              fontFamily='Oswald'
              fontWeight={500}
            >
              NEWS
            </Text>
          </StyledWrapLogo>

          <StyledSearch placeholder='Search...' />
        </RowBetween>
      </ContainerSite>

      <StypedWrapMenu>
        <ContainerSite>
          <StyledWrapListMenu>
            <StyledNav>Home</StyledNav>
            <StyledNav>Blockchain</StyledNav>
            <StyledNav>NFT</StyledNav>
            <StyledNav>MEME Coin</StyledNav>
            <StyledNav>Trending</StyledNav>
            <StyledNav>Web 3</StyledNav>
            <StyledNav>Press Release</StyledNav>

            <RowFixed>
              <Button width='100%' mr='24px'>
                <Text color='#FFF' fontSize='18px'>
                  LAUNCH APP
                </Text>
              </Button>
              <Image src='/images/small-logo.png' width={32} height={29} />
            </RowFixed>
          </StyledWrapListMenu>
        </ContainerSite>
      </StypedWrapMenu>
    </Wrapper>
  );
};

const Wrapper = styled(Box)`
  margin: 0 auto;
`;

const StyledWrapLogo = styled(RowFixed)`
  width: 100%;
  flex: 1;
`;

const StyledSearch = styled(Input)`
  max-width: 400px;
  border-radius: 30px;
  border: 1px solid rgba(15, 15, 15, 0.6);
`;

const StypedWrapMenu = styled(RowMiddle)`
  background: #141517;
  padding: 8px 0;
  width: 100%;
`;

const StyledWrapListMenu = styled(Grid)`
  width: 100%;
  grid-template-columns: repeat(8, auto);
  align-items: center;
`;

const StyledNav = styled(Text)`
  text-transform: uppercase;
  font-size: 18px;
  color: #fff;
`;

const StyledSocial = styled(Box)`
  padding: 8px 0;

  box-shadow: 0px 1px 10px 0px rgba(56, 56, 56, 0.15);
`;

const StyledLogoSocial = styled(Image)`
  width: 24px;
  height: 24px;
`;
export default TopBar;
