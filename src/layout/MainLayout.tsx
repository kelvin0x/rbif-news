import Box from 'components/Box/Box';
import React from 'react';
import { styled } from 'styled-components';
import TopBar from './components/Topbar';

const MainLayout: React.FC<React.PropsWithChildren> = ({ children }) => {
  return (
    <Wrapper>
      <TopBar />
      <StyledWrapContent>{children}</StyledWrapContent>
    </Wrapper>
  );
};

const Wrapper = styled(Box)`
  max-width: 1920px;
  margin: 0 auto;
`;
const StyledWrapContent = styled(Box)``;

export default MainLayout;
