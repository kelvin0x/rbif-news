import styled, { DefaultTheme } from 'styled-components';
import { space, typography, layout } from 'styled-system';
import { TextProps } from './types';

interface ThemedProps extends TextProps {
  theme: DefaultTheme;
}

const getColor = ({ color, theme }: ThemedProps) => {
  return theme.colors?.[color] || color;
};

const getFontSize = ({ fontSize, small }: TextProps) => {
  return small ? '12px' : fontSize || '14px';
};

const getLineHeight = ({ lineHeight }: TextProps) => {
  return lineHeight || 1.2;
};

export const Text = styled.div<TextProps>`
  color: ${getColor};
  font-size: ${getFontSize};
  font-weight: ${({ bold }) => (bold ? 600 : 400)};
  line-height: ${getLineHeight};

  ${({ textTransform }) => textTransform && `text-transform: ${textTransform};`}
  ${({ ellipsis }) =>
    !!ellipsis &&
    `
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: ${ellipsis};
    -webkit-box-orient: vertical;
    `}

  ${space}
  ${typography}
  ${layout}
`;

Text.defaultProps = {
  color: 'text',
  small: false,
};

export default Text;
