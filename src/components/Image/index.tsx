import React from 'react';
import { styled } from 'styled-components';
import { space } from 'styled-system';

const Image: React.FC<{
  src: any;
  width?: number;
  height?: number;
  alt?: string;
}> = ({ src, alt, width, height, ...props }) => {
  return (
    <StyledWrapper $width={width} $height={height} {...props}>
      <StyledImage src={src} alt={alt} />
    </StyledWrapper>
  );
};

const StyledImage = styled.img`
  height: 100%;
  left: 0;
  position: absolute;
  top: 0;
  width: 100%;
`;

const StyledWrapper = styled.div<{ $width: number; $height: number }>`
  max-height: ${({ $height }) => $height}px;
  max-width: ${({ $width }) => $width}px;
  position: relative;
  width: 100%;

  &:after {
    content: '';
    display: block;
    padding-top: ${({ $width, $height }) => ($height / $width) * 100}%;
  }

  ${space}
`;

export default Image;
