import React, { cloneElement, ElementType, isValidElement } from 'react';
import StyledButton from './StyledButton';
import { ButtonProps, variants } from './types';

const Button = <E extends ElementType = 'button'>(
  props: ButtonProps<E>
): JSX.Element => {
  const {
    startIcon,
    endIcon,
    external,
    className,
    isLoading,
    disabled,
    children,
    ...rest
  } = props;

  return (
    <StyledButton {...rest}>
      <>
        {isValidElement(startIcon) &&
          cloneElement(startIcon, {
            style: { marginRight: '0.5rem' },
          } as any)}
        {children}
        {isValidElement(endIcon) &&
          cloneElement(endIcon, {
            style: { marginLeft: '0.5rem' },
          } as any)}
      </>
    </StyledButton>
  );
};

Button.defaultProps = {
  isLoading: false,
  external: false,
  variant: variants.PRIMARY,
  disabled: false,
};

export default Button;
