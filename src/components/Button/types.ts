import { ElementType, ReactNode } from 'react';
import { LayoutProps, SpaceProps } from 'styled-system';
import { Colors, Radii } from 'theme/types';
import { PolymorphicComponentProps } from 'utils/polymorphic';

export const variants = {
  PRIMARY: 'primary',
} as const;

export type Variant = (typeof variants)[keyof typeof variants];

export interface BaseButtonProps extends LayoutProps, SpaceProps {
  as?: 'a' | 'button' | ElementType;
  borderRadius?: keyof Radii;
  external?: boolean;
  isLoading?: boolean;
  variant?: Variant;
  disabled?: boolean;
  startIcon?: ReactNode;
  endIcon?: ReactNode;
  background?: Colors | string;
}

export type ButtonProps<P extends ElementType = 'button'> =
  PolymorphicComponentProps<P, BaseButtonProps>;
