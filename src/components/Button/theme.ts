import theme from 'theme';
import { variants } from './types';

export const styleVariants = {
  [variants.PRIMARY]: {
    background: theme.colors.primary,
    color: 'text',
  },
};
