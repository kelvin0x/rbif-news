import MainLayout from 'layout/MainLayout';
import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Home from 'views/home';

const AppRouter: React.FC = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          path='/'
          element={
            <MainLayout>
              <Home />
            </MainLayout>
          }
        />
      </Routes>
    </BrowserRouter>
  );
};

export default AppRouter;
