import Box from 'components/Box/Box';
import Column from 'components/Box/Column';
import Grid from 'components/Box/Grid';
import { RowFixed } from 'components/Box/Row';
import Image from 'components/Image';
import Text from 'components/Text/Text';
import React from 'react';
import { styled } from 'styled-components';
import { ContainerSite, LineStroke } from 'theme/styled';

const CardNewMini = () => (
  <StyledWrapCardMini>
    <Image src='/images/posts/mini-post.png' width={130} height={112} />
    <Column ml='16px'>
      <Text color='primary' bold>
        NFT
      </Text>
      <Text small display='inline-flex' my='8px'>
        Craig Bator -
        <Text small color='textSubtle' ml='4px'>
          27 Dec 2020
        </Text>
      </Text>
      <Text fontWeight={700} fontSize='16px'>
        Amanda Seyfried became obsessed’ with ghost stories
      </Text>
    </Column>
  </StyledWrapCardMini>
);

const Home = () => {
  return (
    <StyledWrapper>
      <Box position='relative'>
        <StyledMainPost
          width={1920}
          height={777.333}
          src='/images/posts/hot-news.png'
        />

        <StyledWrapInfo>
          <Text color='#FFF'>Craig Bator - 27 Dec 2020</Text>
          <Text color='#FFF' mt='8px' fontWeight={500} fontSize='36px'>
            After all is said and done, more is done
          </Text>
        </StyledWrapInfo>
      </Box>

      <StyledWrapNewInfo>
        <ContainerSite
          display='flex'
          style={{
            alignItems: 'center',
          }}
        >
          <StyledHotNews>
            <Box width='17px'>
              <Image src='/images/icons/fire.png' width={17} height={24} />
            </Box>
            <Text
              ml='8px'
              color='primary'
              style={{
                whiteSpace: 'nowrap',
              }}
              fontWeight={500}
            >
              Hot News
            </Text>
          </StyledHotNews>
          <Text ml='16px' color='#FFF'>
            Indonesia says located black box recorders from crashed plane
            Indonesia says located black box recorders from crashed plane
          </Text>
        </ContainerSite>
      </StyledWrapNewInfo>

      <ContainerSite>
        <Grid gridTemplateColumns='1fr 1fr 1fr' gridGap='24px' my='24px'>
          <CardNewMini />
          <CardNewMini />
          <CardNewMini />
        </Grid>
      </ContainerSite>

      <ContainerSite>
        <StyledWrapLayout>
          <StyledWrapMainPost position='relative' my='24px'>
            <RowFixed>
              <Text fontSize='24px' color='primary' fontWeight={500} mr='8px'>
                RGI Airdrop Event
              </Text>
              <Text fontSize='24px'>- Massive Rewards For All Readers</Text>
            </RowFixed>

            <LineStroke my='16px' background='#393939' />

            <Box position='relative' minHeight='400px'>
              <StyledMainPostEvent
                width={988}
                height={400}
                src='/images/posts/hot-news.png'
              />

              <Box position='absolute' bottom='50px' left='24px' maxWidth='70%'>
                <Text color='#FFF'>Craig Bator - 27 Dec 2020</Text>
                <Text color='#FFF' mt='8px' fontWeight={500} fontSize='36px'>
                  Play This Game for Free on Epic Store This Weekend
                </Text>
              </Box>
            </Box>
          </StyledWrapMainPost>
        </StyledWrapLayout>
      </ContainerSite>
    </StyledWrapper>
  );
};

const StyledWrapper = styled(Box)`
  position: relative;
`;
const StyledMainPost = styled(Image)`
  width: 100%;
  height: 100%;
`;

const StyledMainPostEvent = styled(Image)`
  width: 100%;
  height: 400px;
`;

const StyledWrapInfo = styled(ContainerSite)`
  position: absolute;
  bottom: 50px;
  left: 50%;
  transform: translateX(-50%);
`;

const StyledWrapNewInfo = styled(Box)`
  width: 100%;
  padding: 16px 0;
  background: ${({ theme }) => theme.colors.primary};
`;

const StyledHotNews = styled(Box)`
  width: max-content;
  display: flex;
  align-items: center;

  background: #fff;

  padding: 8px 16px;
  max-width: 100px;
  width: 100%;
`;

const StyledWrapCardMini = styled(RowFixed)`
  background: #fff;
  box-shadow: 0px 0px 8px 0px rgba(0, 0, 0, 0.12);
`;

const StyledWrapLayout = styled(Grid)`
  grid-template-columns: 2.05fr 1fr;
  grid-gap: 22px;
`;
const StyledWrapMainPost = styled(Box)``;
export default Home;
