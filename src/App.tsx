import React from 'react';
import AppRouter from './AppRouter';
import Providers from './Providers';

const App: React.FC = () => {
  return (
    <Providers>
      <AppRouter />
    </Providers>
  );
};
export default App;

