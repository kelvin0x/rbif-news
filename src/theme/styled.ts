import Box from 'components/Box/Box';
import styled from 'styled-components';
import { Colors } from './types';

export const LineStroke = styled(Box)`
  height: 1px;
  background: ${({ theme, background = 'textDisabled' }) =>
    theme.colors[background as keyof Colors] || background};
`;
export const ContainerSite = styled(Box)`
  max-width: ${({ theme }) => theme.siteWidth}px;
  width: 100%;
  margin: 0 auto;
  position: relative;
`;
