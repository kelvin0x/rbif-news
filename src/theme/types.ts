export type Breakpoints = string[];

export type MediaQueries = {
  xs: string;
  sm: string;
  md: string;
  lg: string;
  xl: string;
  xxl: string;
};

export type Radii = {
  small: string;
  default: string;
  larger: string;
};

export type Colors = {
  primary: string;

  text: string;
  textSubtle: string;
};

export type ZIndices = {
  menu: number;
  modal: number;
};

export interface AppTheme {
  siteWidth: number;
  topbarHeight: number;

  breakpoints: Breakpoints;
  mediaQueries: MediaQueries;

  radii: Radii;
  colors: Colors;
  zIndices: ZIndices;
}
