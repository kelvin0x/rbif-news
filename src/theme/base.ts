import { colors } from './colors';
import { MediaQueries, Breakpoints, Radii, ZIndices } from './types';

export const breakpointMap: { [key: string]: number } = {
  xs: 370,
  sm: 576,
  md: 852,
  lg: 968,
  xl: 1080,
  xxl: 1200,
};

const breakpoints: Breakpoints = Object.values(breakpointMap).map(
  (breakpoint) => `${breakpoint}px`
);

const mediaQueries: MediaQueries = {
  xs: `@media screen and (min-width: ${breakpointMap.xs}px)`,
  sm: `@media screen and (min-width: ${breakpointMap.sm}px)`,
  md: `@media screen and (min-width: ${breakpointMap.md}px)`,
  lg: `@media screen and (min-width: ${breakpointMap.lg}px)`,
  xl: `@media screen and (min-width: ${breakpointMap.xl}px)`,
  xxl: `@media screen and (min-width: ${breakpointMap.xxl}px)`,
};

const radii: Radii = {
  small: '4px',
  default: '8px',
  larger: '16px',
};

const zIndices: ZIndices = {
  menu: 90,
  modal: 100,
};

const base = {
  siteWidth: 1340,
  topbarHeight: 70,

  breakpoints,
  mediaQueries,

  radii,
  zIndices,
  colors,
};

export default base;
