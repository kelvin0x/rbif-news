import { DefaultTheme } from 'styled-components';
import base from './base';

const theme: DefaultTheme = {
  ...base,
};

export default theme;
