import { Colors } from './types';

export const colors: Colors = {
  primary: '#0093FE',

  text: '#000',
  textSubtle: '#393939',
};
